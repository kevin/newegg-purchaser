# Setup

## Install Dependencies

```
PUPPETEER_PRODUCT=firefox npm install puppeteer
```

## Setup Configuration

1. `cp config.example.json config.json`
2. Edit `config.json`:
  * `username`: newegg username
  * `password`: newegg password
  * `cv2`: code on the back of your credit card
  * `item_page`: the part of the URL (starting with a slash) for the product after `https://www.newegg.ca` (eg: `/amd-ryzen-9-5900x/p/N82E16819113664`)
  * `refresh_time`: amount of time between page refreshes (with a random variance)
  * `refresh_tries`: the number of times the script will check before quitting (set to 0 for unlimited)
  * `start_time`: a 24 hour time in the format `7:00` that determines when the script will start checking for the day
  * `end_time`: a 24 hour time in the format `20:00` that determines when the script will stop checking for the day
  * `auto_submit`: set this to false if you want to do a test run (everything up to the actual purchase)
  * `price_limit`: set this to a number you don't want your cart total to exceed before checking out

## Credits

Inspired by and some code borrowed from: https://github.com/Ataraksia/NeweggBot
